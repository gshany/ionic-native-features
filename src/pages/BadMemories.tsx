import React, { useContext } from "react";
import MemoriesContent from "../components/MemoriesContent";
import MemoriesContext, { MemoriesMood } from "../context/MemoriesContext";

const BadMemories: React.FC = () => {
  const memoriesCtx = useContext(MemoriesContext);

  const BadMemories = memoriesCtx.memories.filter(
    (memory) => memory.mood === MemoriesMood.bad
  );

  return (
    <MemoriesContent
      title="Bad Memories"
      fallbackText="No bad memories found"
      memories={BadMemories}
    />
  );
};

export default BadMemories;
