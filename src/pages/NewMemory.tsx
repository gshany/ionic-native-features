import React, { useContext, useRef, useState } from "react";

import {
  IonBackButton,
  IonButton,
  IonButtons,
  IonCol,
  IonContent,
  IonGrid,
  IonHeader,
  IonInput,
  IonItem,
  IonLabel,
  IonPage,
  IonRow,
  IonSelect,
  IonSelectOption,
  IonTitle,
  IonToolbar
} from "@ionic/react";
import { Plugins, FilesystemDirectory } from "@capacitor/core";
import { useHistory } from "react-router-dom";
import { base64FromPath } from "@ionic/react-hooks/filesystem";

import MemoriesContext, { MemoriesMood } from "../context/MemoriesContext";
import PhotoPicker, { Photo } from "../components/PhotoPicker";

const NewMemory: React.FC = () => {
  const memoriesCtx = useContext(MemoriesContext);
  const { Filesystem } = Plugins;
  const history = useHistory();

  const [takenPhoto, setTakenPhoto] = useState<Photo>();

  const [memoryMood, setMemoryMood] = useState<MemoriesMood>(MemoriesMood.good);

  const memoryTitleRef = useRef<HTMLIonInputElement>(null);

  const saveMemoryHandler = async () => {
    const memoryTitle = memoryTitleRef.current?.value;

    if (
      !memoryTitle ||
      memoryTitle.toString().trim().length === 0 ||
      !memoryMood ||
      !takenPhoto
    ) {
      return;
    }

    memoriesCtx.createNewMemory(takenPhoto, memoryTitle.toString(), memoryMood);
    history.length > 0 ? history.goBack() : history.replace("/");
  };

  const handleMoodChange = (event: CustomEvent) => {
    const memoryMood = event.detail.value;
    setMemoryMood(memoryMood);
  };

  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonButtons slot="start">
            <IonBackButton defaultHref="/good-memories" />
          </IonButtons>
          <IonTitle>Add New Memory</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonGrid>
          <IonRow>
            <IonCol>
              <IonItem>
                <IonLabel position="floating">Memory Title</IonLabel>
                <IonInput ref={memoryTitleRef}></IonInput>
              </IonItem>
            </IonCol>
          </IonRow>
          <IonRow>
            <IonCol>
              <IonSelect value={memoryMood} onIonChange={handleMoodChange}>
                <IonSelectOption value="good">Good Memory</IonSelectOption>
                <IonSelectOption value="bad">Bad Memory</IonSelectOption>
              </IonSelect>
            </IonCol>
          </IonRow>
          <PhotoPicker onImagePick={setTakenPhoto} />
          <IonRow className="ion-margin-top">
            <IonCol className="ion-text-center">
              <IonButton onClick={saveMemoryHandler}>
                <IonLabel>Add Memory</IonLabel>
              </IonButton>
            </IonCol>
          </IonRow>
        </IonGrid>
      </IonContent>
    </IonPage>
  );
};

export default NewMemory;
