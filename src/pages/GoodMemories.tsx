import React, { useContext } from "react";
import MemoriesContent from "../components/MemoriesContent";
import MemoriesContext, { MemoriesMood } from "../context/MemoriesContext";

const GoodMemories: React.FC = () => {
  const memoriesCtx = useContext(MemoriesContext);

  const goodMemories = memoriesCtx.memories.filter(
    (memory) => memory.mood === MemoriesMood.good
  );

  return (
    <MemoriesContent
      title="Good Memories"
      fallbackText="No good memories found"
      memories={goodMemories}
    />
  );
};

export default GoodMemories;
