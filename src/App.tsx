import React, { useContext, useEffect } from "react";
import { IonApp } from "@ionic/react";

/* Core CSS required for Ionic components to work properly */
import "@ionic/react/css/core.css";

/* Basic CSS for apps built with Ionic */
import "@ionic/react/css/normalize.css";
import "@ionic/react/css/structure.css";
import "@ionic/react/css/typography.css";

/* Optional CSS utils that can be commented out */
import "@ionic/react/css/padding.css";
import "@ionic/react/css/float-elements.css";
import "@ionic/react/css/text-alignment.css";
import "@ionic/react/css/text-transformation.css";
import "@ionic/react/css/flex-utils.css";
import "@ionic/react/css/display.css";

/* Theme variables */
import "./theme/variables.css";
import "./theme/theme.css";

import MemoriesTabs from "./components/MemoriesTabs";
import { IonReactRouter } from "@ionic/react-router";
import MemoriesContext from "./context/MemoriesContext";
import * as ServiceWorker from "./serviceWorker";

const App: React.FC = () => {
  const memoriesCtx = useContext(MemoriesContext);

  const { initContext } = memoriesCtx;

  useEffect(() => {
    initContext();
  }, [initContext]);

  return (
    <IonApp>
      <IonReactRouter>
        <MemoriesTabs />
      </IonReactRouter>
    </IonApp>
  );
};

ServiceWorker.register();

export default App;
