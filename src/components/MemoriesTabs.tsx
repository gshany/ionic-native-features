import {
  IonIcon,
  IonLabel,
  IonRouterOutlet,
  IonSpinner,
  IonTabBar,
  IonTabButton,
  IonTabs
} from "@ionic/react";
import { happyOutline, sadOutline } from "ionicons/icons";
import React from "react";
import { Redirect, Route } from "react-router";
import BadMemories from "../pages/BadMemories";
import GoodMemories from "../pages/GoodMemories";

const NewMemory = React.lazy(() => import("../pages/NewMemory"));

const MemoriesTabs: React.FC = () => {
  return (
    <React.Suspense fallback={<IonSpinner />}>
      <IonTabs>
        <IonRouterOutlet id="main">
          <Route path="/good-memories">
            <GoodMemories />
          </Route>
          <Route path="/bad-memories">
            <BadMemories />
          </Route>
          <Route path="/new-memory">
            <NewMemory />
          </Route>
          <Redirect path="/" to="good-memories" exact />
        </IonRouterOutlet>
        <IonTabBar slot="bottom" defaultValue="good-memories">
          <IonTabButton href="/bad-memories" tab="bad-memories">
            <IonIcon icon={sadOutline}></IonIcon>
            <IonLabel>Bad Memories</IonLabel>
          </IonTabButton>
          <IonTabButton href="/good-memories" tab="good-memories">
            <IonIcon icon={happyOutline}></IonIcon>
            <IonLabel>Good Memories</IonLabel>
          </IonTabButton>
        </IonTabBar>
      </IonTabs>
    </React.Suspense>
  );
};

export default MemoriesTabs;
