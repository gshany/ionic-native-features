import { IonCol, IonRow } from "@ionic/react";
import React from "react";
import { Memory } from "../context/MemoriesContext";
import MemoryItem from "./MemoryItem";

interface MemoriesListProps {
  memories: Memory[];
}

const MemoriesList: React.FC<MemoriesListProps> = (props) => {
  return (
    <>
      {props.memories.map((memory) => (
        <IonRow key={memory.id}>
          <IonCol>
            <MemoryItem title={memory.title} base64Data={memory.base64Data} />
          </IonCol>
        </IonRow>
      ))}
    </>
  );
};

export default MemoriesList;
