import {
  IonButton,
  IonButtons,
  IonContent,
  IonFab,
  IonFabButton,
  IonGrid,
  IonHeader,
  IonIcon,
  IonPage,
  IonTitle,
  IonToolbar,
  isPlatform
} from "@ionic/react";
import { add } from "ionicons/icons";
import React from "react";
import MemoriesContext, { Memory } from "../context/MemoriesContext";
import FixedFabButton from "./FixedFabButton";
import MemoriesList from "./MemoriesList";
import ToolbarAction from "./ToolbarAction";

interface MemoriesContentProps {
  title: string;
  fallbackText: string;
  memories: Memory[];
}

const MemoriesContent: React.FC<MemoriesContentProps> = (props) => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          {isPlatform("ios") && <ToolbarAction icon={add} link={"/new-memory"} />}
          <IonTitle>{props.title}</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonGrid>
          {props.memories.length === 0 ? (
            <h2>{props.fallbackText}</h2>
          ) : (
            <MemoriesList memories={props.memories} />
          )}
        </IonGrid>
        {!isPlatform("ios") && <FixedFabButton icon={add} link={"/new-memory"} />}
      </IonContent>
    </IonPage>
  );
};

export default MemoriesContent;
