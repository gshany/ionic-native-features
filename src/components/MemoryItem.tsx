import React from "react";
import { IonCard, IonCardHeader, IonCardTitle } from "@ionic/react";
import "./MemoryItem.css";

export interface MemoryItemProps {
  title: string;
  base64Data: string;
}

const MemoryItem: React.FC<MemoryItemProps> = (props) => {
  return (
    <IonCard className="memory-card">
      <img src={props.base64Data} alt={props.title} />
      <IonCardHeader>
        <IonCardTitle>{props.title}</IonCardTitle>
      </IonCardHeader>
    </IonCard>
  );
};

export default MemoryItem;
