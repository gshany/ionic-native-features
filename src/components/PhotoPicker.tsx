import React, { useRef, useState } from "react";

import { Camera, CameraResultType, CameraSource, Capacitor } from "@capacitor/core";
import { IonButton, IonCol, IonIcon, IonLabel, IonRow } from "@ionic/react";
import { camera } from "ionicons/icons";

import "./PhotoPicker.css";

export interface Photo {
  path: string | undefined;
  preview: string;
}

interface PhotoPickerProps {
  onImagePick: (photo: Photo) => void;
}

const PhotoPicker: React.FC<PhotoPickerProps> = (props) => {
  const [takenPhoto, setTakenPhoto] = useState<Photo>();

  const fileSelectRef = useRef<HTMLInputElement>(null);

  const handleNoCameraDeviceFound = () => {
    fileSelectRef.current?.click();
  };

  const openFilePicker = (event: React.ChangeEvent<HTMLInputElement>) => {
    const file = event.target!.files![0];
    const fr = new FileReader();

    fr.onload = (event) => {
      const photo: Photo = {
        path: undefined,
        preview: event.target!.result!.toString()
      };

      setTakenPhoto(photo);
      props.onImagePick(photo);
    };

    fr.readAsDataURL(file);
  };

  const takePhotoHandler = async () => {
    if (!Capacitor.isPluginAvailable("Camera")) {
      handleNoCameraDeviceFound();
      return;
    }

    try {
      const photo = await Camera.getPhoto({
        width: 150,
        quality: 80,
        resultType: CameraResultType.Uri,
        source: CameraSource.Camera
      });

      if (!photo || !photo.webPath) {
        return;
      }

      const pickedPhoto: Photo = {
        path: photo.path,
        preview: photo.webPath
      };

      setTakenPhoto(pickedPhoto);
      props.onImagePick(pickedPhoto);
    } catch (error) {
      handleNoCameraDeviceFound();
    }
  };

  return (
    <>
      <IonRow className="ion-text-center">
        <IonCol>
          <div className="photo-container">
            {takenPhoto ? (
              <img src={takenPhoto.preview} alt={takenPhoto.path} />
            ) : (
              <h6>no photo chosen.</h6>
            )}
          </div>
          <IonButton fill="clear" onClick={takePhotoHandler}>
            <IonIcon slot="start" icon={camera} />
            <IonLabel>take photo</IonLabel>
          </IonButton>
        </IonCol>
      </IonRow>
      <input type="file" hidden ref={fileSelectRef} onChange={openFilePicker} />
    </>
  );
};

export default PhotoPicker;
