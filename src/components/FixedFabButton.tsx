import { IonFab, IonFabButton, IonIcon } from "@ionic/react";
import React from "react";

interface FixedFabButtonProps {
  link: string;
  icon: string;
}

const FixedFabButton: React.FC<FixedFabButtonProps> = (props) => {
  return (
    <IonFab horizontal="end" vertical="bottom" slot="fixed">
      <IonFabButton routerLink={props.link}>
        <IonIcon icon={props.icon} />
      </IonFabButton>
    </IonFab>
  );
};

export default FixedFabButton;
