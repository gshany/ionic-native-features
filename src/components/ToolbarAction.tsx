import { IonButton, IonButtons, IonIcon } from "@ionic/react";
import React from "react";

interface ToolbarActionProps {
  link: string;
  icon: string;
}

const ToolbarAction: React.FC<ToolbarActionProps> = (props) => {
  return (
    <IonButtons slot="end">
      <IonButton routerLink={props.link}>
        <IonIcon icon={props.icon} />
      </IonButton>
    </IonButtons>
  );
};

export default ToolbarAction;
