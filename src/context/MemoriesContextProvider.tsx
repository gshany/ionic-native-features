import React, { useState, useEffect, useCallback } from "react";
import MemoriesContext, { Memory, MemoryMood } from "./MemoriesContext";
import { Storage, Filesystem, FilesystemDirectory } from "@capacitor/core";
import { Photo } from "../components/PhotoPicker";
import { base64FromPath } from "@ionic/react-hooks/filesystem";

const MemoriesContextProvider: React.FC = (props) => {
  const MEMORIES_KEY = "memories";

  const [memories, setMemories] = useState<Memory[]>([]);

  useEffect(() => {
    const storedMemories = memories.map((memory) => ({
      id: memory.id,
      imagePath: memory.imagePath,
      mood: memory.mood,
      title: memory.title
    }));

    Storage.set({ key: MEMORIES_KEY, value: JSON.stringify(storedMemories) });
  }, [memories]);

  const createNewMemory = async (takenPhoto: Photo, title: string, mood: MemoryMood) => {
    const fileName = new Date().getTime() + ".jpeg";

    const base64Data = await base64FromPath(takenPhoto!.preview);
    Filesystem.writeFile({
      path: fileName,
      data: base64Data,
      directory: FilesystemDirectory.Data
    });

    const newMemory: Memory = {
      id: Math.random().toString(),
      title,
      imagePath: fileName,
      mood,
      base64Data
    };

    setMemories((currMemories) => {
      return [...currMemories, newMemory];
    });
  };

  const initContext = useCallback(async () => {
    const storedMemories = await Storage.get({ key: MEMORIES_KEY });
    const memoriesData = storedMemories.value ? JSON.parse(storedMemories.value) : [];
    const loadedMemories: Memory[] = [];

    for (const memory of memoriesData) {
      const file = await Filesystem.readFile({
        path: memory.imagePath,
        directory: Filesystem.DEFAULT_DIRECTORY
      });

      loadedMemories.push({
        id: memory.id,
        imagePath: memory.imagePath,
        title: memory.title,
        mood: memory.mood,
        base64Data: "data:image/jpeg;base64," + file.data
      });
    }

    setMemories(loadedMemories);
  }, []);

  return (
    <MemoriesContext.Provider value={{ memories, createNewMemory, initContext }}>
      {props.children}
    </MemoriesContext.Provider>
  );
};

export default MemoriesContextProvider;
