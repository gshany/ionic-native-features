import React from "react";
import { Photo } from "../components/PhotoPicker";

export enum MemoriesMood {
  good = "good",
  bad = "bad"
}

export type MemoryMood = MemoriesMood.good | MemoriesMood.bad;

export interface Memory {
  imagePath: string;
  title: string;
  id: string;
  mood: MemoryMood;
  base64Data: string;
}

const MemoriesContext = React.createContext<{
  memories: Memory[];
  createNewMemory: (takenPhoto: Photo, imagePath: string, mood: MemoryMood) => void;
  initContext: () => void;
}>({
  memories: [],
  createNewMemory: () => {},
  initContext: () => {}
});

export default MemoriesContext;
